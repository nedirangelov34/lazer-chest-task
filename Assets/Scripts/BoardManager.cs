﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class BoardManager : MonoBehaviour
{
    public static BoardManager Instance { set; get; }
    private bool[,] allowedMoves { set; get; }
    private bool[,] allowedAttackArea { set; get; }

    public Dragon[,] Dragons { set; get; }

    public ArrayList Drones { set; get; }
    public ArrayList Dreadnoughts { set; get; }
    public ArrayList Bosses { set; get; }

    private Dragon selectedDragon;
    private int damageToTake;

    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = 0.5f;

    public const int BOARD_SIZE = 8;

    private int selectionX = -1;
    private int selectionY = -1;
    private int levelChosen = 0;

    //orientetion
    private Quaternion normalOrientation = Quaternion.Euler(0, 180, 0);
    private Quaternion rotatedOrientation = Quaternion.Euler(0, 0, 0);

    public List<GameObject> dragonPrefabs;
    private List<GameObject> activeDragons = new List<GameObject>();

    public bool isPlayerTurn = true;

    public Button endTurnButton;
    public Button attackButton;
    public TextMeshProUGUI informationText;
    public TextMeshProUGUI combatLogText;

    // Start is called before the first frame update
    void Start()
    {


        Instance = this;

        if (PlayerPrefs.HasKey("currentLevel"))
        {
            levelChosen = PlayerPrefs.GetInt("currentLevel");
            switch (levelChosen)
            {
                case 1:
                    SpawnAllDragonsForLevel1();
                    break;
                case 2:
                    SpawnAllDragonsForLevel2();
                    break;
                case 3:
                    SpawnAllDragonsForLevel3();
                    break;
                case 4:
                    SpawnAllDragonsForLevel4();
                    break;
            }
        }

        endTurnButton.onClick.AddListener(EndTheTurn);
        attackButton.onClick.AddListener(AttackAllowedDragons);
        attackButton.gameObject.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        UpdateSelection();
        DrawChessboard();
        if (Input.GetMouseButtonDown(0) && isPlayerTurn)
        {
            if (selectionX >= 0 && selectionY >= 0)
            {
                if (selectedDragon == null)
                {
                    SelectDragon(selectionX, selectionY);
                }
                else
                {
                    MoveDragon(selectionX, selectionY);
                }
            }
        }
    }

    private void StartNextRound()
    {
        isPlayerTurn = !isPlayerTurn;

        if (isPlayerTurn)
        {
            informationText.text = "It's your turn ! Please move your dragons.";
            combatLogText.text = combatLogText.text + "\nIt's your turn ! ";
        }
    }

    private void SelectDragon(int x, int y)
    {
        if (Dragons[x, y] == null)
            return;

        if (Dragons[x, y].isPlayer != isPlayerTurn)
            return;


        //Check if we have at least one move, or else uncecessary calculation
        bool hasAtLeastOneMove = false;
        allowedMoves = Dragons[x, y].PossibleMove();
        for (int i = 0; i < BOARD_SIZE; i++)
        {
            for (int j = 0; j < BOARD_SIZE; j++)
            {
                if (allowedMoves[i, j])
                {
                    hasAtLeastOneMove = true;
                }
            }
        }
        if (!hasAtLeastOneMove)
            return;

        selectedDragon = Dragons[x, y];

        if (selectedDragon.HasMoved)
        {
            combatLogText.text = combatLogText.text + "\nSelected Dragon has already moved for this round ! ";

            selectedDragon = null;
            return;
        }
        BoardHighlights.Instance.MoveHighlightAllowedMoves(allowedMoves);
    }

    private void MoveDragon(int x, int y)
    {
        if (selectedDragon != null && selectedDragon.HasMoved)
        {
            return;
        }
        //In case we try to move outside the game field
        if (x < 0 || x > 7 || y < 0 || y > 7)
        {
            return;
        }
        if (selectedDragon.CurrentX == x && selectedDragon.CurrentY == y)
        {

            combatLogText.text = combatLogText.text + "\nYou need to move";
            return;
        }
        if (allowedMoves[x, y] == true)
        {
            Dragons[selectedDragon.CurrentX, selectedDragon.CurrentY] = null;
            selectedDragon.transform.position = GetTileCenter(x, y);
            selectedDragon.SetPosition(x, y);
            Dragons[x, y] = selectedDragon;
            Dragons[x, y].HasMoved = true;
            damageToTake = Dragons[x, y].Damage;
            combatLogText.text = combatLogText.text + "\nMoving " + selectedDragon.GetType();
        }

        BoardHighlights.Instance.HideMoveHighlights();
        BoardHighlights.Instance.HideAttackHighlights();
        //if you click outside the area unselect
        selectedDragon = null;


        //ATTACK
        //Find attack area and see if any enemies are there
        if (Dragons[x, y] != null)
        {
            allowedAttackArea = Dragons[x, y].PossibleAttack();
            BoardHighlights.Instance.AttackHighlightAllowedMoves(allowedAttackArea);

            if (!isPlayerTurn)
                return;

            //Check if we have any elements in the allowedAttackArea and show attack button if we do.
            for (int i = 0; i < BOARD_SIZE; i++)
            {
                for (int j = 0; j < BOARD_SIZE; j++)
                {
                    if (allowedAttackArea[i, j])
                    {
                        attackButton.gameObject.SetActive(true);
                        break;
                    }
                }
            }
        }


        //Check closest monster from the list
    }

    private void DrawChessboard()
    {
        Vector3 widthLine = Vector3.right * BOARD_SIZE;
        Vector3 heightLine = Vector3.forward * BOARD_SIZE;

        for (int i = 0; i <= BOARD_SIZE; i++)
        {
            Vector3 start = Vector3.forward * i;
            Debug.DrawLine(start, start + widthLine);
            for (int j = 0; j <= BOARD_SIZE; j++)
            {
                start = Vector3.right * j;
                Debug.DrawLine(start, start + heightLine);
            }
        }
    }

    private void UpdateSelection()
    {

        if (!Camera.main)
            return;

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("ChessPlane")))
        {
            selectionX = (int)hit.point.x;
            selectionY = (int)hit.point.z;

        }
    }


    private void SpawnDragons(int index, int x, int y)
    {

        GameObject go;

        go = Instantiate(dragonPrefabs[index], GetTileCenter(x, y), normalOrientation) as GameObject;
        go.transform.SetParent(transform);
        Dragons[x, y] = go.GetComponent<Dragon>();
        Dragons[x, y].SetPosition(x, y);
        activeDragons.Add(go);
    }

    private Vector3 GetTileCenter(int x, int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (TILE_SIZE * x) + TILE_OFFSET;
        origin.z += (TILE_SIZE * y) + TILE_OFFSET;

        return origin;
    }

    private void SpawnAllDragonsForLevel1()
    {

        activeDragons = new List<GameObject>();
        Dragons = new Dragon[BOARD_SIZE, BOARD_SIZE];
        //Spawn the Player Team
        SpawnDragons(0, 1, 0);
        SpawnDragons(2, 3, 0);
        //Spawn the AI Team
        SpawnDragons(3, 5, 3);
        SpawnDragons(3, 4, 3);
        SpawnDragons(5, 2, 7);

    }

    private void SpawnAllDragonsForLevel2()
    {

        activeDragons = new List<GameObject>();
        Dragons = new Dragon[BOARD_SIZE, BOARD_SIZE];
        ////Spawn the Player Team
        SpawnDragons(0, 1, 0);
        SpawnDragons(0, 4, 0);
        SpawnDragons(1, 7, 0);
        //Spawn the AI Team
        SpawnDragons(3, 5, 7);
        SpawnDragons(4, 6, 6);
        SpawnDragons(5, 4, 7);

    }
    private void SpawnAllDragonsForLevel3()
    {

        activeDragons = new List<GameObject>();
        Dragons = new Dragon[BOARD_SIZE, BOARD_SIZE];
        ////Spawn the Player Team
        SpawnDragons(0, 1, 0);
        SpawnDragons(1, 4, 0);
        SpawnDragons(2, 7, 0);
        //Spawn the AI Team
        SpawnDragons(3, 1, 6);
        SpawnDragons(3, 2, 6);
        SpawnDragons(3, 3, 6);
        SpawnDragons(4, 5, 7);
        SpawnDragons(5, 7, 7);

    }

    private void SpawnAllDragonsForLevel4()
    {

        activeDragons = new List<GameObject>();
        Dragons = new Dragon[BOARD_SIZE, BOARD_SIZE];
        //Spawn the Player Team
        SpawnDragons(0, 3, 2);
        SpawnDragons(0, 4, 2);
        SpawnDragons(0, 5, 2);
        SpawnDragons(1, 5, 1);
        SpawnDragons(2, 4, 0);
        //Spawn the AI Team
        SpawnDragons(3, 1, 6);
        SpawnDragons(3, 2, 6);
        SpawnDragons(3, 3, 6);
        SpawnDragons(4, 4, 7);
        SpawnDragons(4, 5, 7);
        SpawnDragons(5, 7, 7);
        SpawnDragons(5, 1, 6);

    }
    private void EndGame()
    {
        foreach (GameObject dragon in activeDragons)
        {
            Destroy(dragon);
        }
        isPlayerTurn = true;
        BoardHighlights.Instance.HideAttackHighlights();
        BoardHighlights.Instance.HideMoveHighlights();

        //Open Next Level
    }

    void EndTheTurn()
    {

        attackButton.gameObject.SetActive(false);
        StartNextRound();

        int numberOfPlayerUnits = 0;
        //Count how many player units we have and if we have 0, end the game.
        foreach (Dragon dragon in Dragons)
        {
            if (dragon != null && dragon.isPlayer == true)
            {
                numberOfPlayerUnits++;
            }
        }
        if (numberOfPlayerUnits == 0)
        {

            //Player Loses
            endLevel(false);
        }

        if (isPlayerTurn)
        {
            endTurnButton.gameObject.SetActive(true);
            return;
        }
        else
        {
            endTurnButton.gameObject.SetActive(false);
        }

        Drones = new ArrayList();
        Dreadnoughts = new ArrayList();
        Bosses = new ArrayList();

        foreach (Dragon dragon in Dragons)
        {
            if (dragon is Dragon)
            {
                dragon.HasMoved = false;

                if (dragon is Drone)
                {
                    Drones.Add(dragon);
                }
                if (dragon is Dreadnought)
                {
                    Dreadnoughts.Add(dragon);
                }
                if (dragon is Boss)
                {
                    Bosses.Add(dragon);
                }
            }
        }



        //The Drone has to move first.
        StartCoroutine(delayedDroneAIAction());




    }

    private void DroneAIAction(Dragon dragon)
    {
        if (dragon is Drone)
        {
            selectedDragon = dragon;

            if (!isPlayerTurn)
            {
                allowedMoves = selectedDragon.PossibleMove();
                for (int i = 0; i < BOARD_SIZE; i++)
                {
                    for (int j = 0; j < BOARD_SIZE; j++)
                    {
                        //There is only 1 move possible for Drone so we just move it with that forward.
                        if (allowedMoves[i, j])
                        {

                            SelectDragon(i, j);
                            MoveDragon(i, j);
                            AttackAllowedDragons();
                            //StartCoroutine(startDelayedActions(i,j));

                        }
                    }
                }
            }
        }
    }
    private void DreadnoughtAIAction(Dragon dragon)
    {

        if (dragon is Dreadnought)
        {
            selectedDragon = dragon;

            if (!isPlayerTurn)
            {

                Dragon closestEnemyDragon = null;


                bool[,] findClosestDragonArray = findClosestDragon();
                for (int i = 0; i < BOARD_SIZE; i++)
                {
                    for (int j = 0; j < BOARD_SIZE; j++)
                    {

                        if (findClosestDragonArray[i, j])
                        {
                            closestEnemyDragon = Dragons[i, j];
                        }
                    }
                }


                if (closestEnemyDragon == null)
                    return;

                SelectDragon(selectedDragon.CurrentX, selectedDragon.CurrentY);
                //move the selected dragon closer to the closest enemy dragon by 1 every turn.
                if (closestEnemyDragon.CurrentX <
                    selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY < selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY - 1);
                }
                else if (closestEnemyDragon.CurrentX > selectedDragon.CurrentX &&
                    closestEnemyDragon.CurrentY > selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY + 1);
                }
                else if (closestEnemyDragon.CurrentX == selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY < selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX, selectedDragon.CurrentY - 1);
                }
                else if (closestEnemyDragon.CurrentX < selectedDragon.CurrentX &&
                 closestEnemyDragon.CurrentY > selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY + 1);
                }
                else if (closestEnemyDragon.CurrentX > selectedDragon.CurrentX &&
                 closestEnemyDragon.CurrentY < selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY - 1);
                }
                else if (closestEnemyDragon.CurrentX == selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY > selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX, selectedDragon.CurrentY + 1);
                }
                else if (closestEnemyDragon.CurrentX < selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY == selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY);
                    //++both;
                }
                else if (closestEnemyDragon.CurrentX > selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY == selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY);
                }

                AttackAllowedDragons();

                return;

            }
        }
    }
    private void BossAIAction(Dragon dragon)
    {

        if (dragon is Boss)
        {

            selectedDragon = dragon;

            if (!isPlayerTurn)
            {

                Dragon closestEnemyDragon = null;


                bool[,] findClosestDragonArray = findClosestDragon();
                for (int i = 0; i < BOARD_SIZE; i++)
                {
                    for (int j = 0; j < BOARD_SIZE; j++)
                    {

                        if (findClosestDragonArray[i, j])
                        {
                            closestEnemyDragon = Dragons[i, j];
                        }
                    }
                }


                if (closestEnemyDragon == null)
                    return;
                SelectDragon(selectedDragon.CurrentX, selectedDragon.CurrentY);
                //move the selected dragon closer to the closest enemy dragon by 1 every turn.
                if (closestEnemyDragon.CurrentX < selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY < selectedDragon.CurrentY)
                {
                    if (selectedDragon.CurrentX != 7)
                    {
                        MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY);
                    }
                    else
                    {
                        MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY);
                    }
                }
                else if (closestEnemyDragon.CurrentX > selectedDragon.CurrentX &&
                    closestEnemyDragon.CurrentY > selectedDragon.CurrentY)
                {
                    if (selectedDragon.CurrentX != 0)
                    {
                        MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY);
                    }
                    else
                    {
                        MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY);
                    }
                }
                else if (closestEnemyDragon.CurrentX == selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY < selectedDragon.CurrentY)
                {
                    if (selectedDragon.CurrentX == 0)
                    {
                        MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY);
                    }
                    else
                    {
                        MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY);
                    }
                }
                else if (closestEnemyDragon.CurrentX < selectedDragon.CurrentX &&
                 closestEnemyDragon.CurrentY > selectedDragon.CurrentY)
                {
                    MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY);
                }
                else if (closestEnemyDragon.CurrentX > selectedDragon.CurrentX &&
                 closestEnemyDragon.CurrentY < selectedDragon.CurrentY)
                {
                    if (selectedDragon.CurrentX > 0)
                    {
                        MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY);
                    }
                }
                else if (closestEnemyDragon.CurrentX == selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY > selectedDragon.CurrentY)
                {
                    if (selectedDragon.CurrentX == 0)
                    {
                        MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY);
                    }
                    else
                    {
                        MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY);
                    }
                }
                else if (closestEnemyDragon.CurrentX < selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY == selectedDragon.CurrentY)
                {
                    if (selectedDragon.CurrentX != 7)
                    {
                        MoveDragon(selectedDragon.CurrentX + 1, selectedDragon.CurrentY);
                    }

                }
                else if (closestEnemyDragon.CurrentX > selectedDragon.CurrentX &&
                   closestEnemyDragon.CurrentY == selectedDragon.CurrentY)
                {
                    if (selectedDragon.CurrentX != 0)
                    {
                        MoveDragon(selectedDragon.CurrentX - 1, selectedDragon.CurrentY);
                    }
                }


            }
        }
    }

    IEnumerator delayedDroneAIAction()
    {
        informationText.text = "Selecting Drone to Move and Attack";
        foreach (Dragon dr in Drones)
        {
            DroneAIAction(dr);
            yield return new WaitForSeconds(2f);
        }

        //The Dreadnought action is second.
        StartCoroutine(delayedDreadnoughtAIAction());
    }
    IEnumerator delayedDreadnoughtAIAction()
    {
        informationText.text = "Selecting Dreadnought to Move and Attack";
        foreach (Dragon dr in Dreadnoughts)
        {
            DreadnoughtAIAction(dr);
            yield return new WaitForSeconds(2f);
        }

        //The Boss action is second.    
        StartCoroutine(delayedBossAIAction());

    }
    IEnumerator delayedBossAIAction()
    {
        informationText.text = "Selecting Boss to Move and Attack";
        foreach (Dragon dr in Bosses)
        {
            yield return new WaitForSeconds(2f);
            BossAIAction(dr);
        }
        EndTheTurn();
    }

    private bool[,] findClosestDragon()
    {

        bool[,] r = new bool[BOARD_SIZE, BOARD_SIZE];

        Dragon dragon;

        int xs = selectedDragon.CurrentX, ys = selectedDragon.CurrentY; // Start coordinates

        for (int d = 1; d < BOARD_SIZE * 2 + 1; d++)
        {
            for (int i = 0; i < d + 1; i++)
            {
                int x1 = xs - d + i;
                int y1 = ys - i;

                if ((x1 >= 0 && x1 < BOARD_SIZE) && (y1 >= 0 && y1 < BOARD_SIZE))
                {
                    dragon = BoardManager.Instance.Dragons[x1, y1];
                    if (dragon != null && dragon.isPlayer)
                        r[x1, y1] = true;
                }
                int x2 = xs + d - i;
                int y2 = ys + i;

                if ((x2 >= 0 && x2 < BOARD_SIZE) && (y2 >= 0 && y2 < BOARD_SIZE))
                {
                    dragon = BoardManager.Instance.Dragons[x2, y2];
                    if (dragon != null && dragon.isPlayer)
                        r[x2, y2] = true;
                }
            }

            for (int i = 1; i < d; i++)
            {
                int x1 = xs - i;
                int y1 = ys + d - i;

                if ((x1 >= 0 && x1 < BOARD_SIZE) && (y1 >= 0 && y1 < BOARD_SIZE))
                {
                    dragon = BoardManager.Instance.Dragons[x1, y1];
                    if (dragon != null && dragon.isPlayer)
                        r[x1, y1] = true;
                }
                int x2 = xs + i;
                int y2 = ys - d + i;
                if ((x2 >= 0 && x2 < BOARD_SIZE) && (y2 >= 0 && y2 < BOARD_SIZE))
                {
                    dragon = BoardManager.Instance.Dragons[x2, y2];
                    if (dragon != null && dragon.isPlayer)
                        r[x2, y2] = true;
                }
            }
        }
        return r;
    }

    void AttackAllowedDragons()
    {

        for (int i = 0; i < BOARD_SIZE; i++)
        {
            for (int j = 0; j < BOARD_SIZE; j++)
            {
                if (allowedAttackArea[i, j])
                {

                    //This check is when AI finds a target that is another AI
                    if (!isPlayerTurn)
                    {
                        if (!Dragons[i, j].isPlayer)
                        {
                            break;
                        }
                    }


                    combatLogText.text = combatLogText.text + "\n" + Dragons[i, j].GetType() + " took " + damageToTake + " damage";
                    Dragons[i, j].Health -= damageToTake;
                    if (Dragons[i, j].Health <= 0)
                    {

                        combatLogText.text = combatLogText.text + "\n" + Dragons[i, j].GetType() + " is dead !";

                        if (Dragons[i, j] is Boss &&
                            Bosses.Count == 1 && Dragons[i, j].Health <= 0)
                        {
                            //Player Wins
                            endLevel(true);
                        }

                        //Remove the dragon as it has 0 health and is dead.
                        Dragon dragon = Dragons[i, j];
                        activeDragons.Remove(dragon.gameObject);
                        Destroy(dragon.gameObject);
                        Dragons[i, j] = null;



                    }
                    attackButton.gameObject.SetActive(false);
                    selectedDragon = null;
                }
            }
        }
        allowedAttackArea = null;

    }
    public void endLevel(bool winner)
    {

        int currentLevel = PlayerPrefs.GetInt("currentLevel");

        if (winner)
        {
            if (currentLevel == 1)
            {
                PlayerPrefs.SetInt("levelReached", 2);
            }
            else if (currentLevel == 2)
            {
                PlayerPrefs.SetInt("levelReached", 3);
            }
            else if (currentLevel == 3)
            {
                PlayerPrefs.SetInt("levelReached", 4);
            }
        }

        EndGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);


    }
}
