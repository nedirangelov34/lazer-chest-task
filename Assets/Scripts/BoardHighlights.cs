﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardHighlights : MonoBehaviour
{
    public static BoardHighlights Instance { set; get; }

    public GameObject MoveHighlightPrefab;
    public GameObject AttackHighlightPrefab;
    private List<GameObject> MoveHighlights;
    private List<GameObject> AttackHighlights;


    private void Start()
    {
        Instance = this;
        MoveHighlights = new List<GameObject>();
        AttackHighlights = new List<GameObject>();
    }

    private GameObject GetMoveHighlightObject() {
        GameObject go = MoveHighlights.Find(g => !g.activeSelf);
        if (go == null)
        {
            go = Instantiate(MoveHighlightPrefab);
            MoveHighlights.Add(go);
        }
        return go;
    }
    private GameObject GetAttackHighlightObject()
    {
        GameObject go = AttackHighlights.Find(g => !g.activeSelf);
        if (go == null)
        {
            go = Instantiate(AttackHighlightPrefab);
            AttackHighlights.Add(go);
        }
        return go;
    }

    public void MoveHighlightAllowedMoves(bool[,] moves) {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (moves[i,j]) {
                    GameObject go = GetMoveHighlightObject();
                    go.SetActive(true);
                    go.transform.position = new Vector3(i+0.5f, 0, j + 0.5f);
                }
            }
        }
    }
    public void AttackHighlightAllowedMoves(bool[,] moves)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (moves[i, j])
                {
                    GameObject go = GetAttackHighlightObject();
                    go.SetActive(true);
                    go.transform.position = new Vector3(i + 0.5f, 0, j + 0.5f);
                }
            }
        }
    }

    public void HideMoveHighlights()
    {
        foreach (GameObject go in MoveHighlights)
        {
            go.SetActive(false);
        }
    }
    public void HideAttackHighlights()
        {
            foreach (GameObject go in AttackHighlights){
            go.SetActive(false);
        }
    }
}
