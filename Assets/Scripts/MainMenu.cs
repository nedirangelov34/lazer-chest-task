﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject level2Button;
    public GameObject level3Button;
    public GameObject level4Button;
    private void Start()
    {
        //Reset Player Prefs For testing Purposes only
       //PlayerPrefs.DeleteAll();

        switch (PlayerPrefs.GetInt("levelReached",1))
        {
            case 4:
                level4Button.SetActive(true);
                goto case 3;
            case 3:
                level3Button.SetActive(true);
                goto case 2;
            case 2:
                level2Button.SetActive(true);
                break;
        }
        

    }

    public void StarLevel1() {
        PlayerPrefs.SetInt("currentLevel", 1);
        LoadGameScene();
    }
    public void StarLevel2()
    {
        if (PlayerPrefs.GetInt("levelReached") >= 2) { 
            PlayerPrefs.SetInt("currentLevel", 2);
            LoadGameScene();
        }
    }
    public void StarLevel3()
    {
        if (PlayerPrefs.GetInt("levelReached") >=3){
            PlayerPrefs.SetInt("currentLevel", 3);
            LoadGameScene();
        }
    }
    public void StarLevel4()
    {
        if (PlayerPrefs.GetInt("levelReached") >= 4){
            PlayerPrefs.SetInt("currentLevel", 4);
            LoadGameScene();
        }
    }
    private void LoadGameScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void QuitGame() {
        Application.Quit();
    }

}
