﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : Dragon
{
    private const int HEALTH = 4;
    private const int DAMAGE = 2;

    Tank()
    {
        this.Health = HEALTH;
        this.Damage = DAMAGE;
    }

    private const int maxLengthOfAMovement = 3;

    //Moves like the Queen in chess, up to a maximum of 3 spaces.
    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];

        Dragon dragon;
        
        //Right
        for (int i = CurrentX+1; i <= CurrentX + maxLengthOfAMovement; i++)
        {
            if (i >= BoardManager.BOARD_SIZE)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[i, CurrentY];
            if (dragon == null)
                r[i, CurrentY] = true;
            else
            {
                // if there is another dragon in range break the cicle
                //i = CurrentX + maxLengthOfAMovement+1;
                break;
            }

        }

        //Left
        for (int i = CurrentX-1; i >= CurrentX - maxLengthOfAMovement; i--)
        {
            if (i < 0)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[i, CurrentY];
            if (dragon == null)
                r[i, CurrentY] = true;
            else
            {
                // if there is another dragon in range break the cicle
                //i = CurrentX - maxLengthOfAMovement - 1;
                break;
            }
        }

        //TOP
        for (int i = CurrentY+1; i <= CurrentY + maxLengthOfAMovement; i++)
        {

            if (i >= BoardManager.BOARD_SIZE)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[CurrentX, i];
            if (dragon == null)
            {
                r[CurrentX, i] = true;
            }
            else
            {
                // if there is another dragon in range break the cicle
                break;
            }
        }

        //Down
        for (int i = CurrentY-1; i >= CurrentY - maxLengthOfAMovement; i--)
        {
            if (i < 0)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[CurrentX, i];
            if (dragon == null)
                r[CurrentX, i] = true;
            else
            {
                // if there is another dragon in range break the cicle
                //i = CurrentY - maxLengthOfAMovement - 1;
                break;
            }
        }

        //Top Left Diagonal
        if (CurrentX != 0 && CurrentY != 7)
        {
            for (int i = 1; i < maxLengthOfAMovement+1; i++)
            {

                if ((CurrentX - i) >= 0 && (CurrentY + i) <= 7)
                {
                    dragon = BoardManager.Instance.Dragons[CurrentX - i, CurrentY + i];
                    //Making sure there is a Dragon there
                    if (dragon == null )
                    {
                        r[CurrentX - i, CurrentY + i] = true;


                    }
                    else
                    {
                        // if there is another dragon in range break the cicle
                        if (CurrentX != dragon.CurrentX && CurrentY != dragon.CurrentY)
                        {
                            break;
                        }
                    }
                }
            }
        }

        //Top Right Diagonal
        if (CurrentX != 7 && CurrentY != 7)
        {
            for (int i = 1; i < maxLengthOfAMovement+1; i++)
            {
                if ((CurrentX + i) <= 7 && (CurrentY + i) <= 7)
                {
                    dragon = BoardManager.Instance.Dragons[CurrentX + i, CurrentY + i];
                    //Making sure there is a Dragon there
                    if (dragon == null)
                    {
                        r[CurrentX + i, CurrentY + i] = true;
                    }
                    else
                    {
                        // if there is another dragon in range break the cicle
                        if (CurrentX != dragon.CurrentX && CurrentY != dragon.CurrentY)
                        {
                            break;
                        }
                    }
                }
            }
        }


        //Bottom Left Diagonal
        if (CurrentX != 0 && CurrentY != 0)
        {
            for (int i = 1; i < maxLengthOfAMovement+1; i++)
            {
                if ((CurrentX - i) >= 0 && (CurrentY - i) >= 0)
                {
                    dragon = BoardManager.Instance.Dragons[CurrentX - i, CurrentY - i];
                    //Making sure there is a Dragon there
                    if (dragon == null)
                    {
                        r[CurrentX - i, CurrentY - i] = true;
                    }
                    else
                    {
                        // if there is another dragon in range break the cicle
                        if (CurrentX != dragon.CurrentX && CurrentY != dragon.CurrentY)
                        {
                            break;
                        }
                    }
                }
            }
        }

        //Bottom Right Diagonal
        if (CurrentX != BoardManager.BOARD_SIZE-1 && CurrentY != 0)
        {
            for (int i = 1; i < maxLengthOfAMovement+1; i++)
            {
                if ((CurrentX + i) <= BoardManager.BOARD_SIZE-1 && (CurrentY - i) >= 0)
                {
                    dragon = BoardManager.Instance.Dragons[CurrentX + i, CurrentY - i];
                    //Making sure there is a Dragon there
                    if (dragon == null)
                    {
                        r[CurrentX + i, CurrentY - i] = true;
                    }
                    else
                    {
                        // if there is another dragon in range break the cicle
                        if (CurrentX != dragon.CurrentX && CurrentY != dragon.CurrentY)
                        {
                            break;
                        }
                    }
                }
            }
        }
        return r;
    }



    //Diagonal Attack in 4 directions at any range
    public override bool[,] PossibleAttack()
    {
        bool[,] r = new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];

        Dragon dragon;
        int i;

        //Right
        i = CurrentX;
        while (true)
        {
            i++;
            if (i >= BoardManager.BOARD_SIZE)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[i, CurrentY];
            if (dragon != null)
                if (!dragon.isPlayer) { 
                    r[i, CurrentY] = true;
                    return r;
                }

        }

        //Left
        i = CurrentX;
        while (true)
        {
            i--;
            if (i < 0)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[i, CurrentY];
            if (dragon != null)
                if (!dragon.isPlayer) { 
                    r[i, CurrentY] = true;
                return r;
            }

    }

        //Top
        i = CurrentY;
        while (true)
        {
            i++;
            if (i >= BoardManager.BOARD_SIZE)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[CurrentX, i];
            if (dragon != null)
                if (!dragon.isPlayer) {
                    r[CurrentX, i] = true;
                    return r;
                }

        }

        //Down
        i = CurrentY;
        while (true)
        {
            i--;
            if (i < 0)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[CurrentX, i];
            if (dragon != null)
                if (!dragon.isPlayer) { 
                    r[CurrentX, i] = true;
                    return r;
                }

        }

        return r;
    }

}
