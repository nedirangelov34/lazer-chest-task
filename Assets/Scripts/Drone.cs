﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : Dragon
{
    private const int HEALTH = 2;
    private const int DAMAGE = 1;

    Drone()
    {
        this.Health = HEALTH;
        this.Damage = DAMAGE;
    }


    // Moves forward 1 space from its side of the board (like a pawn, but never moves diagonally).
    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];

        Dragon dragon;
        //Move Down
        if (CurrentY == 1) { 
            //Player Lost
            BoardManager.Instance.endLevel(false);
        }
        else if (CurrentY != 0)
        {
            dragon = BoardManager.Instance.Dragons[CurrentX, CurrentY - 1];
            if (dragon == null)
            {
                r[CurrentX, CurrentY - 1] = true;
            }
        }
       
        return r;
    }



    //Diagonal Attack in 4 directions at any range
    public override bool[,] PossibleAttack()
    {
        bool[,] r = new bool[8, 8];

        Dragon dragon;


        //Top Left Diagonal
        if (CurrentX != 0 && CurrentY != 7)
        {
            for (int i = 1; i < 7; i++)
            {

                if ((CurrentX - i) >= 0 && (CurrentY + i) <= 7)
                {
                    dragon = BoardManager.Instance.Dragons[CurrentX - i, CurrentY + i];
                    //Making sure there is a Dragon there
                    if (dragon != null && dragon.isPlayer)
                    {
                        r[CurrentX - i, CurrentY + i] = true;
                        //prioritaze top left diagonal 
                        return r;
                    }
                }
            }
        }

        //Top Right Diagonal
        if (CurrentX != 7 && CurrentY != 7)
        {
            for (int i = 1; i < 7; i++)
            {
                if ((CurrentX + i) <= 7 && (CurrentY + i) <= 7)
                {
                    dragon = BoardManager.Instance.Dragons[CurrentX + i, CurrentY + i];
                    //Making sure there is a Dragon there
                    if (dragon != null && dragon.isPlayer)
                    {
                        r[CurrentX + i, CurrentY + i] = true;
                        return r;
                    }
                }
            }
        }

        //Bottom Left Diagonal
        if (CurrentX != 0 && CurrentY != 0)
        {
            for (int i = 1; i < 7; i++)
            {
                if ((CurrentX - i) >= 0 && (CurrentY - i) >= 0)
                {
                    dragon = BoardManager.Instance.Dragons[CurrentX - i, CurrentY - i];
                    //Making sure there is a Dragon there
                    if (dragon != null && dragon.isPlayer)
                    {
                        r[CurrentX - i, CurrentY - i] = true;
                        return r;
                    }
                }
            }
        }

        //Bottom Right Diagonal
        if (CurrentX != 7 && CurrentY != 0)
        {
            for (int i = 1; i < 7; i++)
            {
                if ((CurrentX + i) <= 7 && (CurrentY - i) >= 0)
                {
                    dragon = BoardManager.Instance.Dragons[CurrentX + i, CurrentY - i];
                    //Making sure there is a Dragon there
                    if (dragon != null && dragon.isPlayer)
                    {
                        r[CurrentX + i, CurrentY - i] = true;
                        return r;
                    }
                }
            }
        }

       

        return r;
    }
}
