﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Dragon
{

    private const int HEALTH = 5;
    private const int DAMAGE = 0;

    Boss()
    {
        this.Health = HEALTH;
        this.Damage = DAMAGE;
    }

    //Moves 1 space in any direction:
    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];

        Dragon dragon;
        //Move Left
        if (CurrentX != 0)
        {
            dragon = BoardManager.Instance.Dragons[CurrentX - 1, CurrentY ];
            if (dragon == null)
            {
                r[CurrentX - 1, CurrentY] = true;
            }
        }

        //Move Right 
        if (CurrentX != 7)
        {
            dragon = BoardManager.Instance.Dragons[CurrentX + 1, CurrentY];
            if (dragon == null)
            {
                r[CurrentX + 1, CurrentY] = true;
              }
        }
        return r;
    }

}
