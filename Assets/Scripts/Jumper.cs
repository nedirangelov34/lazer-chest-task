﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : Dragon
{

    private const int HEALTH = 2;
    private const int DAMAGE = 2;

    Jumper()
    {
        this.Health = HEALTH;
        this.Damage = DAMAGE;
    }


    //Move orthogonally in 4 directions
    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];

        //Up Left
        JumperMove(CurrentX - 1, CurrentY + 2, ref r);

        //Up Right
        JumperMove(CurrentX + 1, CurrentY + 2, ref r);

        //Right Up
        JumperMove(CurrentX + 2, CurrentY + 1, ref r);

        //Right Down
        JumperMove(CurrentX + 2, CurrentY - 1, ref r);

        //Down Left
        JumperMove(CurrentX - 1, CurrentY - 2, ref r);

        //Down Right
        JumperMove(CurrentX + 1, CurrentY - 2, ref r);

        //Left Up
        JumperMove(CurrentX - 2, CurrentY + 1, ref r);

        //Left Down
        JumperMove(CurrentX - 2, CurrentY - 1, ref r);

        return r;
    }

    public void JumperMove(int x, int y, ref bool[,] r)
    {

        Dragon dragon;
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            dragon = BoardManager.Instance.Dragons[x, y];
            if (dragon == null)
                r[x, y] = true;
        }
    }


    //Diagonal Attack in 4 directions at any range
    public override bool[,] PossibleAttack()
    {
        bool[,] r = new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];

        Dragon dragon;

        int maxLenghtOfAttack = 5;

        //Right
        for (int i = CurrentX; i < CurrentX + maxLenghtOfAttack; i++)
        {
            if (i >= 8)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[i, CurrentY];
            if (dragon != null)
                if (!dragon.isPlayer)
                {
                    r[i, CurrentY] = true;
                    break;
                }
        }

        //Left
        for (int i = CurrentX; i > CurrentX - maxLenghtOfAttack; i--)
        {
            if (i < 0)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[i, CurrentY];
            if (dragon != null)
                if (!dragon.isPlayer)
                {
                    r[i, CurrentY] = true;
                    break;
                }

        }

        //TOP
        for (int i = CurrentY; i < CurrentY + maxLenghtOfAttack; i++)
        {

            if (i >= 8)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[CurrentX, i];
            if (dragon != null)
                if (!dragon.isPlayer)
                {
                    r[CurrentX, i] = true;
                    break;
                }

        }

        //Down
        for (int i = CurrentY; i > CurrentY - maxLenghtOfAttack; i--)
        {
            if (i < 0)
            {
                break;
            }
            dragon = BoardManager.Instance.Dragons[CurrentX, i];
            if (dragon != null)
                if (!dragon.isPlayer)
                {
                    r[CurrentX, i] = true;
                    break;
                }
        }

        return r;
    }
}
