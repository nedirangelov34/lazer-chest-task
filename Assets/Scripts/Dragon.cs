﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Dragon : MonoBehaviour
{
    public int CurrentX { set; get; }
    public int CurrentY { set; get; }
    public bool isPlayer;

    public int Health { set; get; }
    public int Damage { set; get; }

    public bool HasMoved = false; 

    public void SetPosition(int x, int y) {

        CurrentX = x;
        CurrentY = y;

    }



    public virtual bool[,] PossibleMove() {

        return new bool [BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];
    }
    public virtual bool[,] PossibleAttack()
    {
        return new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];
    }

}
