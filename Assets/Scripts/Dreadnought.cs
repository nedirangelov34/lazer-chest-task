﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dreadnought : Dragon
{
    private const int HEALTH = 5;
    private const int DAMAGE = 2;

    Dreadnought()
    {
        this.Health = HEALTH;
        this.Damage = DAMAGE;
    }


    //Moves 1 space in any direction:
    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];

        Dragon dragon;

        int i, j;

        //Top Side
        i = CurrentX - 1;
        j = CurrentY + 1;
        if (CurrentY != 7) {
            for (int k = 0; k < 3; k++)
            {
                if (i >= 0 && i < 8) {
                    dragon = BoardManager.Instance.Dragons[i, j];
                    if (dragon == null)
                        r[i, j] = true;
                }

                i++;
            }
        }

        //Bottom Side
        i = CurrentX - 1;
        j = CurrentY - 1;
        if (CurrentY != 0)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i >= 0 && i < 8)
                {
                    dragon = BoardManager.Instance.Dragons[i, j];
                    if (dragon == null)
                        r[i, j] = true;
                }

                i++;
            }
        }

        //Middle Left Side
        if (CurrentX != 0)
        {
            dragon = BoardManager.Instance.Dragons[CurrentX-1, CurrentY];
            if (dragon == null)
                r[CurrentX - 1, CurrentY] = true;
        }

        //Middle Left Side
        if (CurrentX != 7)
        {
            dragon = BoardManager.Instance.Dragons[CurrentX + 1, CurrentY];
            if (dragon == null)
                r[CurrentX + 1, CurrentY] = true;
        }


        return r;
    }

    //Diagonal Attack in 4 directions at any range
    public override bool[,] PossibleAttack()
    {
        bool[,] r = new bool[BoardManager.BOARD_SIZE, BoardManager.BOARD_SIZE];

        Dragon dragon;


        int i, j;

        //Top Side
        i = CurrentX - 1;
        j = CurrentY + 1;
        if (CurrentY != 7)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i >= 0 && i < 8)
                {
                    dragon = BoardManager.Instance.Dragons[i, j];
                    if (dragon != null && dragon.isPlayer)
                        r[i, j] = true;
                }

                i++;
            }
        }

        //Bottom Side
        i = CurrentX - 1;
        j = CurrentY - 1;
        if (CurrentY != 0)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i >= 0 && i < 8)
                {
                    dragon = BoardManager.Instance.Dragons[i, j];
                    if (dragon != null && dragon.isPlayer)
                        r[i, j] = true;
                }

                i++;
            }
        }

        //Middle Left Side
        if (CurrentX != 0)
        {
            dragon = BoardManager.Instance.Dragons[CurrentX - 1, CurrentY];
            if (dragon != null && dragon.isPlayer)
                r[CurrentX - 1, CurrentY] = true;
        }

        //Middle Left Side
        if (CurrentX != 7)
        {
            dragon = BoardManager.Instance.Dragons[CurrentX + 1, CurrentY];
            if (dragon != null && dragon.isPlayer)
                r[CurrentX + 1, CurrentY] = true;
        }



        return r;
    }
}
